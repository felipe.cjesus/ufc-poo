
import java.util.Random;

public class Luta {
    //Atributos
    private Lutador desafiado;
    private Lutador desafiante;
    private int rounds;
    private boolean aprovada;
    //Metodos publico
    public void marcarLuta(Lutador l1, Lutador l2){
        if(l1.getCategoria().equals(l2.getCategoria()) && l1 != l2){
            this.aprovada = true;
            this.desafiado = l1;
            this.desafiante = l2;
        }else{
            this.aprovada = false;
            this.desafiado = null;
            this.desafiante = null;
        }
    }
    public void lutar(){
        if(this.aprovada){
            System.out.println("");
            System.out.println("### Desafiado ###");
            this.desafiado.apresentar();
            System.out.println("");
            System.out.println("### Desafiante ###");
            this.desafiante.apresentar();
            
            Random aleatorio = new Random();
            int vencedor = aleatorio.nextInt(3); // 0 1 2 numeros aleatorios da classe RANDOM
            System.out.println("====== RESULTADO DA LUTA ======");
            switch(vencedor){                
            
                case 0: //Empatou 
                System.out.println("Empatou!");
                this.desafiado.empatarLuta();
                this.desafiante.empatarLuta();
                break;
                
                case 1: // Desafiado Ganhou
                System.out.println("Desafiante " +  this.desafiado.getNome() + " ganhou!");
                this.desafiado.ganharLuta();
                this.desafiante.perderLuta();
                break;
                
                case 2: // Desafiante Ganhou
                System.out.println("Desafiante " +  this.desafiante.getNome() + " ganhou!");
                this.desafiado.perderLuta();
                this.desafiante.ganharLuta();
                break;
            }
            System.out.println("================================");
        }else{
            System.out.println("Luta não pode acontecer!!");
        }
    }
    //Metodos especiais
    public Lutador getDesafiado() {
        return desafiado;
    }

    public void setDesafiado(Lutador desafiado) {
        this.desafiado = desafiado;
    }

    public Lutador getDesafiante() {
        return desafiante;
    }

    public void setDesafiante(Lutador desafiante) {
        this.desafiante = desafiante;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public boolean isAprovada() {
        return aprovada;
    }

    public void setAprovada(boolean aprovada) {
        this.aprovada = aprovada;
    }          
}
